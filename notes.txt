1) Initialize node package manager (NPM)

  $ npm init

2) express.js is a third party library (framework)
  install express.js package
    
    $ npm install express
    $ npm i express
  
  // require express module in the project

3) mongoose is a package needs to be installed
   to be used to interact with the server
      reference: https://mongoosejs.com/docs/5.x/docs/guide.html#
	npm i mongoose
    
    $ npm i mongoose

    //require mongoose module in the project
		// see Mongoose (Getting Started) documentation for steps by steps 
		// reference: https://mongoosejs.com/docs/5.x/docs/index.html
  
  // require express module in the project

4) Connect your database in the project
    get connection string from mongoDB Atlas
    use connect() method
      update the password of the connection string
      change the name of the database in the connection string

5) Optional: Add notification of whether database is connected

6) Create a Schema
    > Schema servers as a blueprint of our documents
      reference: https://mongoosejs.com/docs/5.x/docs/index.html

7) Create a Model out of the Schema
  > Why? To make use of Model methods

      findById()
      findOne()
      etc.

8) Create a route
  Note: add parsers in the index.js file to convert JSON payloads to be understood
        by Express framework.
        
    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))

  use Router() to handle requests (userRoutes)
    Syntax:
      router.HTTPmethod('url', <request listener>)

9) Create .gitignore file
    > So that when you push your project in your online repository, 
      other modules not necessary for the project will not be pushed
        
    $ touch .gitignore

10) Optional: install nodemon

    $ npm i nodemon

    add scripts:
      "start": "nodemon index.js"



*****************
git commands:
 git add .
 git commit -m "message."
 git remote add s32 <SSH link of said repo>
 git push s32 master

 (s32-36 repository in gitlab) API DEVELOPMENT
	git commands:
		git init
		git add .
		git commit -m "API DEVELOPMENT"
		git remote add origin <ssh link of s32-36 repo>
		git push origin master

	(s32 repository in gitlab)
	git commands:
		git add .
		git commit -m "activity in s32"
		git remote add s32 <ssh link of s32 repo>
		git push s32 master