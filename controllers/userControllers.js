// business logic
// including model methods

// User model
const { model } = require('mongoose')
const User = require('./../models/User')

module.exports.checkEmail = async (email) => {
  // find the matching document in the databse using email
  // by using Model() method.
  // send back the response to the client
  const result = await User.findOne({ email: email })
  if (result != null) email.send(false)
  return result == null ? email.send(true) : res.send(error)
}

module.exports.registerUser = (reqBody) => {
  // save/create a new user document
  // using .save() method to save document to the database
  // console.log(reqBody)

  // how to use object destructuring
  // why? to make distinct variables for each property w/o using dot notation
  // const {properties} = < object reference >
  const { firstname, lastname, email, password, mobileNo, age } = reqBody
  // console.log(firstName)

  const newUser = new User({
    firstname: firstname,
    lastname: lastname,
    email: email,
    password: password,
    mobileNo: mobileNo,
    age: age
  })

  // save the newUser object to the database
  return newUser.save().then((result, error) => {
    console.log(result)
    // return error ? error : result
    return result ? true : error
  })
}

module.exports.getAllUsers = () => {
  // difference between findOne() & ficnd()
  // findOne() returns one document
  // find(query, fieldprojection) return an array of documents []
  return User.find().then((result) => {
    return result ? result : error
  })
}
