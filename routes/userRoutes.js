// create routes
const express = require('express')

// import to use .Router() method
// because .Router() is an express method
// Router() handles the requests
const router = express.Router()

// User Controller to use invoke()
const userControllers = require('../controllers/userControllers')

/*
syntax: router.method('uri', <request listener>)
router.get('/', (req, res) => {
  // console.log(`Hello from userRoutes 👋`)
  // res.send(`Hello from userRoutes 👋🔥`)
})
router.post('/', (req, res) => {
  console.log(`Hello from postRequest 👋`)
  res.send(`Hello ${req.body.name} 👋🔥`)
})
*/

// s32
// Routes would only handle requests & response

router.get('/get-all-users', (req, res) => {
  userControllers.getAllUsers(req.body).then((result) => res.send(result))
})

router.post('/email-exists', (req, res) => {
  userControllers.checkEmail(req.body.email).then((result) => res.send(result))
})

// register route
router.post('/register', (req, res) => {
  // expecting data/object coming from req body.
  userControllers.registerUser(req.body).then((result) => res.send(result))
})

module.exports = router
